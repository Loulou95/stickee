<?php

class PackSizeManager
{
	public function getPackSizes()
	{
		// this could be implemented using database approach
		// using static array for now
		return [250, 500, 1000, 2000, 5000];
	}
}


class PacksManager
{
	protected $packSizeManager;

	function __construct(PackSizeManager $packSizeManager)
	{
		$this->packSizeManager = $packSizeManager;
	}

	public function calculatePacks($numberOfWidgetsOrdered)
	{
		$packSizes = $this->packSizeManager->getPackSizes();

		// sort packsizes array in desending order
		rsort($packSizes);

		// fill the requied packs array with to default values of 0
		$requiredPacks = array_fill_keys($packSizes, 0);

		foreach ($packSizes as $size) {

			$packs = floor($numberOfWidgetsOrdered / $size);
			if ($packs > 0) {
				$requiredPacks[$size] = $packs;
				$numberOfWidgetsOrdered -= $packs * $size;
			}
		}

		if ($numberOfWidgetsOrdered > 0) $requiredPacks[min($packSizes)]++;

		return $requiredPacks;
	}
}

echo "How many widgets you want to order? ";
$numberOfWidgetsOrdered = rtrim(fgets(STDIN));

$requiredPacks = (new PacksManager(new PackSizeManager))
	->calculatePacks($numberOfWidgetsOrdered);

echo "\n\nTotal widgets ordered: {$numberOfWidgetsOrdered}\n\n";

echo "Number of packs required as below: \n\n";

// print_r($requiredPacks);
foreach ($requiredPacks as $packSize => $numberOfPacks) {
	echo "For pack size: {$packSize} - send {$numberOfPacks} packs\n\n";
}
